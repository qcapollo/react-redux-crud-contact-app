//Sass configuration
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(cb) {
    gulp.src('src/styles/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/styles/css'));
    cb();
});

gulp.task('default', gulp.series('sass', function(cb) {
    gulp.watch('src/styles/scss/*.scss', gulp.series('sass'));
    cb();
}));