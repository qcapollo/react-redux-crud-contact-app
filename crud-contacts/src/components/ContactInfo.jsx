import React from 'react';
import { connect } from 'react-redux';

import '../styles/css/contactInfo.css';

import * as Mode from '../contants/func_modes';
import { switchToNormal } from '../actions/index';
import { 
    resolvedDeleteContact, 
    resolvedLoadAllContacts, 
    resolvedUpdateContact, 
    resolvedLoadContact,
    resolvedCreateContact } from '../actions/call_api_actions';
import { Col, Row, Input, Icon, Button } from 'react-materialize';

const InputIdKey = {
    name: 'fname',
    phone: 'fphone',
    dob: 'fdob',
    mail: 'fmail',
    addr: 'faddress',
    secr: 'fsecret'
}
const initContact = {
    name: '',
    dob: '',
    email: '',
    address: '',
    phone: '',
    secret: ''
}
const mapStateToProps = state => {
    return {
        contact: state.selectedContact,
        mode: state.func_mode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        switchToNormalMode: () => dispatch(switchToNormal()),
        onDeleteContact: contactId => dispatch(resolvedDeleteContact(contactId)),
        loadAllContact: () => dispatch(resolvedLoadAllContacts()),
        onUpdateContact: contactEntries => dispatch(resolvedUpdateContact(contactEntries)),
        loadContact: contactId => dispatch(resolvedLoadContact(contactId)),
        createContact: contactEntries => dispatch(resolvedCreateContact(contactEntries))
    }
}

class ConnectedContactInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info: initContact,
            isEditing: false
        }
        this.handleBeginEdit = this.handleBeginEdit.bind(this);
        this.handleEndEditing = this.handleEndEditing.bind(this);
        this.handleSaveChanges = this.handleSaveChanges.bind(this);
        this.handleDeleteContact = this.handleDeleteContact.bind(this);
        this.handleUpdateInputValue = this.handleUpdateInputValue.bind(this);
    }

    handleUpdateInputValue(e) {
        console.log(`handleUpdateInputValue -> event.target: `, e.target);
        console.log(`handleUpdateInputValue -> event.target.id: `, e.target.id);
        let inputId = e.target.id;
        let value = e.target.value;
        switch (inputId) {
            case InputIdKey.name: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.name = value;
                    return {
                        info: info
                    }
                });
                break;
            }
            case InputIdKey.phone: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.phone = value;
                    return {
                        ...prevState,
                        info: info
                    }
                });
                break;
            }
            case InputIdKey.dob: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.dob = value;
                    return {
                        info: info
                    }
                });
                break;
            }
            case InputIdKey.mail: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.email = value;
                    return {
                        info: info
                    }
                });
                break;
            }
            case InputIdKey.addr: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.address = value;
                    return {
                        info: info
                    }
                });
                break;
            }
            case InputIdKey.secr: {
                this.setState(prevState => {
                    let { info } =  {...prevState};
                    info.secret = value;
                    return {
                        info: info
                    }
                });
                break;
            }
            default: break;
        }
    }

    handleBeginEdit(event) {
        console.log(event.target);
        this.setState({
            isEditing: true
        });
    }

    handleEndEditing(event) {
        console.log(event.target);
        if (this.props.mode === Mode.ADD_CONTACT) {
            this.props.switchToNormalMode();
        }
        this.setState({
            isEditing: false
        });
    }

    async handleSaveChanges(event) {

        if (this.props.mode === Mode.ADD_CONTACT) {
            //Call api to create new contact -> show preloader
            let contactEntries = {
                name: this.state.info.name || '',
                phone: this.state.info.phone || '',
                dob: this.state.info.dob || '',
                email: this.state.info.email || '',
                address: this.state.info.address || '',
            }
            await this.props.createContact(contactEntries);
            await this.props.loadAllContact();

            await this.props.switchToNormalMode();
            this.setState({
                info: {...initContact},
                isEditing: false
            })
            //if success -> switch to Normal, hide preloader, select the lastest contact then show info
            //if fail -> show error message
        }
        else {
            let contactEntries = {
                name: this.state.info.name || this.props.contact.name,
                phone: this.state.info.phone || this.props.contact.phone,
                dob: this.state.info.dob || this.props.contact.dob,
                email: this.state.info.email || this.props.contact.email,
                address: this.state.info.address || this.props.contact.address,
                secret: this.state.info.secret || this.props.contact.secret,
                id: this.props.contact.id
            }

            {/** call api to update info */}



            await this.props.onUpdateContact(contactEntries);
            await this.props.loadAllContact();
            await this.props.loadContact(contactEntries.id);

            this.setState({
                isEditing: false,
                info: { ...initContact }
            });

        }
    }

    async handleDeleteContact(event) {
        alert('Delete contact clicked');
        //Call api to delete selected contact
        await this.props.onDeleteContact(this.props.contact.id);
        await this.props.loadAllContact();
    }

    render() {
        let isCreateMode = this.props.mode === Mode.ADD_CONTACT;
        if (isCreateMode && !this.state.isEditing) {
            this.state.isEditing = true;
        }
        return (
            <div className="container-full">
                <Row>
                    {/* Background View */}
                    <img className='banner' src="./images/contact-banner.png" alt="contact-banner"/>
                </Row>
                <Row>

                    <div className='contact-avatar' >
                        <img src="./images/avatar.jpg" alt="contact-avatar"/>
                    </div>

                </Row>
                <Row>
                    {/* Info view */}
                    <Col s={0} m={2} l={2}>
                        {/* This is left side*/}
                    </Col>
                    <Col s={12} m={8} l={8}>
                        {this.props.contact != null ? (
                            (this.state.isEditing) ? 
                                (
                                    <Row>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.name}
                                            label="Full name" 
                                            defaultValue={this.props.contact.name}
                                            onChange={this.handleUpdateInputValue}>
                                                <Icon center>account_circle</Icon>
                                        </Input>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.phone}
                                            label="Phone"
                                            onChange={this.handleUpdateInputValue}
                                            defaultValue={this.props.contact.phone}>
                                                <Icon center>phone</Icon>
                                        </Input>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.dob}
                                            label="Birthday"
                                            onChange={this.handleUpdateInputValue}
                                            defaultValue={this.props.contact.dob}>
                                                <Icon center>calendar_today</Icon>
                                        </Input>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.mail}
                                            label="Mail"
                                            onChange={this.handleUpdateInputValue}
                                            defaultValue={this.props.contact.email}>
                                                <Icon center>mail</Icon>
                                        </Input>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.addr}
                                            label="Address"
                                            onChange={this.handleUpdateInputValue}
                                            defaultValue={this.props.contact.address}>
                                                <Icon center>map</Icon>
                                        </Input>
                                        <Input 
                                            s={12} m={12} l={6} 
                                            id={InputIdKey.secr}
                                            label="Secret key"
                                            onChange={this.handleUpdateInputValue}
                                            defaultValue={!isCreateMode ? this.props.contact.secret : "[This field will be generated automatically]"}
                                            disabled>
                                                <Icon center>lock</Icon>
                                        </Input>
                                    </Row>
                                ) : (
                                    <Row>
                                        <Col s={12} m={12} l={6} >
                                            <div className="contact-info-entry">
                                                <Icon>account_circle</Icon>
                                                {' '}
                                                <span>{this.props.contact.name}</span>
                                            </div>
                                            <div className="contact-info-entry">
                                                <Icon>phone</Icon>
                                                {' '}
                                                <span>{this.props.contact.phone}</span>
                                            </div>
                                            <div className="contact-info-entry">
                                                <Icon>calendar_today</Icon>
                                                {' '}
                                                <span>{this.props.contact.dob}</span>
                                            </div>
                                        </Col>
                                        <Col s={12} m={12} l={6} >
                                            <div className="contact-info-entry">
                                            <Icon>mail</Icon>
                                            {' '}
                                            <span>{this.props.contact.email}</span>
                                            </div>
                                            <div className="contact-info-entry">
                                                <Icon>map</Icon>
                                                {' '}
                                                <span>{this.props.contact.address}</span>
                                            </div>
                                            <div className="contact-info-entry">
                                                <Icon>lock</Icon>
                                                {' '}
                                                <span>{this.props.contact.secret}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                )
                            ) : (
                                <Row className="guide-text">Select any contact to view detail info!</Row>)
                            }
                    </Col>
                    <Col s={0} m={2} l={2}>
                        {/* This is right side*/}
                        {/* Put icon edit here */}
                    </Col>
                </Row>
                {/* Buttons are placed here */}
                {this.props.contact != null ? (
                        this.state.isEditing ? 
                        (<Row>
                            <div className="save-clear-btn-group">
                                <Button 
                                    large floating 
                                    waves='light' 
                                    className='green'
                                    onClick={this.handleSaveChanges}>
                                        <Icon center>save</Icon>
                                </Button>
                                <Button large floating 
                                    waves='light' 
                                    className='red'
                                    onClick={this.handleEndEditing}>
                                        <Icon center>clear</Icon>
                                </Button>
                            </div>
                        </Row>)
                        : 
                        (
                        <Row>
                            <Col s={12} m={12} l={12}>
                                <div className="edit-delete-button">
                                    <Button 
                                        large floating 
                                        waves='light' 
                                        className='red'
                                        onClick={this.handleDeleteContact}>
                                        <Icon center>delete</Icon>
                                    </Button>
                                    <Button 
                                        large floating 
                                        waves='light' 
                                        className='purple'
                                        onClick={this.handleBeginEdit}>
                                        <Icon center>mode_edit</Icon>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                        )
                    ) : null
                }

            </div>
        );
    }
}

const ContactInfo = connect(mapStateToProps, mapDispatchToProps) (ConnectedContactInfo);
export default ContactInfo;
