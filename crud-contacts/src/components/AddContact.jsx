import React from 'react';
import { connect } from 'react-redux';
import { Button, Icon, Row, Col } from 'react-materialize';

import { showAddContact } from '../actions/index';

const mapDispatchToProps = dispatch => {
    return {
        onClick: () => dispatch(showAddContact())
    }
}

class ConnectedAddContact extends React.Component {
    render () {
        return (
            <Button 
                floating
                large
                waves="light"
                onClick={this.props.onClick}>
                <Icon center large>person_add</Icon>
            </Button>
        );
    }
}

const AddContact = connect(null, mapDispatchToProps) (ConnectedAddContact);
export default AddContact;