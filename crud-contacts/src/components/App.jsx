import React from 'react';
import { connect } from 'react-redux';

import { Row, Col } from 'react-materialize';

import '../styles/css/app.css';

import SearchBar from './SearchBar';
import ContactList from './ContactList';
import ContactInfo from './ContactInfo';
import AddContact from './AddContact';


class ConnectedContactApp extends React.Component {
    render() {
        return (
            <div>
                <Row>
                    <Col s={12} m={12} l={3}
                        className="left-side">
                        <Row>
                           <Col s={12} m={12} l={12}>
                                <div className="app-title">Awesome Contact</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col s={12} m={12} l={12}
                                className="btn-create">
                                <AddContact />
                            </Col>
                        </Row>
                        <Row>
                            <Col s={12} m={12} l={12}>
                                <SearchBar/>
                            </Col>
                        </Row>
                        <Row className="contact-list">
                            <Col s={12} m={12} l={12}>
                                <ContactList/>
                            </Col>
                        </Row>
                    </Col>
                    <Col s={12} m={12} l={9} >
                        {/* Put contact detail in here */}
                        <ContactInfo />
                    </Col>
                </Row>
            </div>
        );
    }
}

const ContactApp = connect(null, null) (ConnectedContactApp);
export default ContactApp;