import * as Actions from '../contants/action_types';

export function addNewContact(payload) {
    return {
        type: Actions.ADD_NEW_CONTACT,
        payload
    }
}

export function loadContact(payload) {
    return {
        type: Actions.LOAD_CONTACT,
        payload
    }
}

export function updateContact(payload) {
    return {
        type: Actions.UPDATE_CONTACT_INFO,
        payload
    }
}

export function deleteContact(payload) {
    return {
        type: Actions.DELETE_CONTACT,
        payload
    }
}

export function searchContact(payload) {
    return {
        type: Actions.SEARCH_CONTACTS,
        payload
    }
}
export function clearSearchText() {
    return {
        type: Actions.CLEAR_SEARCH_TEXT
    }
}

export function showAddContact() {
    return {
        type: Actions.SHOW_ADD_CONTACT
    }
}

export function switchToNormal() {
    return {
        type: Actions.SWITCH_TO_NORMAL
    }
}

