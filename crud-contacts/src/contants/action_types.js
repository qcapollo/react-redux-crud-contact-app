export const ADD_NEW_CONTACT = 'contact/rootReducer/ADD_NEW_CONTACT';
export const LOAD_CONTACT = 'contact/rootReducer/LOAD_CONTACT';
export const UPDATE_CONTACT_INFO = 'contact/rootReducer/UPDATE_CONTACT_INFO';
export const DELETE_CONTACT= 'contact/rootReducer/DELETE_CONTACT';

export const SHOW_ADD_CONTACT = 'contact/rootReducer/SHOW_ADD_CONTACT';
export const SEARCH_CONTACTS = 'contact/rootReducer/SEARCH_CONTACTS';

export const SWITCH_TO_NORMAL = 'contact/rootReducer/SWITCH_TO_NORMAL';
export const CLEAR_SEARCH_TEXT = 'contact/rootReducer/CLEAR_SEARCH_TEXT';

//For api calls
export const RESOLVED_LOAD_ALL_CONTACTS = 'contact/rootReducer/RESOLVED_LOAD_ALL_CONTACTS';
export const RESOLVED_LOAD_CONTACT = 'contact/rootReducer/RESOLVED_LOAD_CONTACT';
export const RESOLVED_UPDATE_CONTACT = 'contact/rootReducer/RESOLVED_UPDATE_CONTACT';
export const RESOLVED_CREATE_CONTACT = 'contact/rootReducer/RESOLVED_CREATE_CONTACT';
export const RESOLVED_DELETE_CONTACT = 'contact/rootReducer/RESOLVED_DELETE_CONTACT';