import React from 'react';
import { connect } from 'react-redux';
import { CollectionItem, Col, Row, Icon } from 'react-materialize';
import '../styles/css/contactItem.css';

import { resolvedLoadContact } from '../actions/call_api_actions';

const mapDispatchToProps = dispatch => {
    return {
        onSelectContact: (contactId) => dispatch(resolvedLoadContact(contactId))
    }
}


 class ConnectedContactListItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        event.stopPropagation();
        this.props.onSelectContact(this.props.id);
    }

    render() {
        return (
            <div className="item-container" onClick={this.handleClick}>
            <CollectionItem className="item">
                <Row className="item-row">
                    <Col s={3} m={3} l={3}>
                        <img style={styles.avatar} src="./images/5024-200.png" alt="avatar"/>
                    </Col>
                    <Col s={9} m={9} l={9}>
                            <span style={styles.contactName}>{this.props.name}</span>
                        <br/>
                        <span>{this.props.phone}</span>
                    </Col>

                    <div className="btn-pick">
                        <Icon center>navigate_next</Icon>
                    </div>
                </Row>
            </CollectionItem>
            </div>
        );
    }
}
//styles

const ContactListItem = connect(null, mapDispatchToProps) (ConnectedContactListItem);
export default ContactListItem;

const styles = {};

styles.contactName = {
    fontWeight: 'bolder'
}

styles.avatar = {
    height: '45px',
    width: '45px'
}