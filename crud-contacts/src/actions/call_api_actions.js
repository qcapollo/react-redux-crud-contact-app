import { 
    RESOLVED_CREATE_CONTACT, 
    RESOLVED_DELETE_CONTACT, 
    RESOLVED_LOAD_ALL_CONTACTS, 
    RESOLVED_LOAD_CONTACT, 
    RESOLVED_UPDATE_CONTACT 
} from '../contants/action_types';

import { SERVER_HOST } from '../contants/config';

//api
export function resolvedLoadAllContacts() {
    return function(dispatch) {
        return fetch(`${SERVER_HOST}/api/contacts`)
            .then(response => response.json())
            .then(json => {
                dispatch({
                    type: RESOLVED_LOAD_ALL_CONTACTS,
                    payload: json
                });
            });
    }
}
export function resolvedLoadContact(contactId) {
    return function(dispatch) {
        return fetch(`${SERVER_HOST}/api/contact/${contactId}`,{
                method: 'GET',
                mode: 'cors'
            })
            .then(response => response.json())
            .then(json => {
                dispatch({
                    type: RESOLVED_LOAD_CONTACT,
                    payload: json.contact
                });
            });
    }
}
export function resolvedUpdateContact(contactEntries) {
    console.log('resolvedUpdateContact -> contactEntries: ', contactEntries);
    return function(dispatch) {
        return fetch(`${SERVER_HOST}/api/contact`,{
            method: 'PATCH',
            mode: 'cors',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(contactEntries)
        })
        .then(response => response.json())
        .then(json => {
            dispatch({
                type: RESOLVED_UPDATE_CONTACT,
                payload: json
            });
        });
    }
}

export function resolvedCreateContact(contactEntries) {
    console.log('resolvedCreateContact -> contactEntries: ', contactEntries);
    return function(dispatch) {
        return fetch(`${SERVER_HOST}/api/contact`,{
            method: 'POST',
            mode: 'cors',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(contactEntries)
        })
        .then(response => response.json())
        .then(json => {
            dispatch({
                type: RESOLVED_CREATE_CONTACT,
                payload: json
            });
        }); 
    }
}

export function resolvedDeleteContact(contactId) {
    return function(dispatch) {
        return fetch(`${SERVER_HOST}/api/contact`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    "id": contactId
                })
            })
            .then(response => response.json())
            .then(json => {
                dispatch({
                    type: RESOLVED_DELETE_CONTACT,
                    payload: json
                });
            });
    }
}