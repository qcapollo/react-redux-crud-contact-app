import * as Actions from '../contants/action_types';
import * as Mode from '../contants/func_modes';

const initContact = {
    name: '',
    dob: '',
    email: '',
    address: '',
    phone: '',
    secret: '',
    id: ''
}

const initialState = {
    contacts: [
        {
            name: 'Nguyen Trong Nghia',
            dob: '12/12/1999',
            email: 'CgYPAw8EDw8@mail.com',
            address: 'Thanh Hoa',
            phone: '07425787191',
            secret: '168trq-22@v#&#XUMHQx',
            id: 'CgYPAw8EDw8',
            
        },
        {
            name: 'Tran Thi B',
            dob: '11/11/1990',
            email: 'DAYACgoKDQ8@mail.com',
            address: 'Vinh Phuc',
            phone: '08002010182',
            secret: 'b%-4#jU!vw9h970%zCSQ',
            id: 'DAYACgoKDQ8'

        },
        {
            name: 'Vo Thanh Nhan',
            dob: '01/02/1998',
            email: 'BwgKAAAHDgg@mail.com',
            address: 'Thanh Hoa',
            phone: '07624050726',
            secret: '70#U%L7d@9F0_mTTjcb#',
            id: 'BwgKAAAHDgg'
        }
    ],
    searchText: '',
    selectedContact: null,
    func_mode: Mode.NORMAL
}

function rootReducer(state = initialState, action) {
    switch(action.type) {
        case Actions.ADD_NEW_CONTACT: {
            return {
                ...state,
                contacts: state.contacts.concat(action.payload),
                func_mode: Mode.NORMAL
            }
        }
        case Actions.LOAD_CONTACT: {
            // let { contactInfo } = action.payload;
            return {
                ...state
            }
        }
        case Actions.UPDATE_CONTACT_INFO: {
            // let contactEntry = action.payload;
            return {
                ...state,
            }
        }
        case Actions.DELETE_CONTACT: {
            return {
                ...state
            }
        }
        case Actions.SEARCH_CONTACTS: {
            let searchText = action.payload;
            return {
                ...state,
                searchText: searchText
            }
        }

        case Actions.CLEAR_SEARCH_TEXT: {
            return {
                ...state,
                searchText: ''
            }
        }
        case Actions.SHOW_ADD_CONTACT: {
            return {
                ...state,
                func_mode: Mode.ADD_CONTACT,
                selectedContact: {...initContact}
            }
        }
        case Actions.SWITCH_TO_NORMAL: {
            return {
                ...state,
                func_mode: Mode.NORMAL,
                selectedContact: null
            }
        }
        case Actions.RESOLVED_LOAD_ALL_CONTACTS: {
            console.log(action.payload)
            return {
                ...state,
                contacts: action.payload.data.slice()
            }
        }
        case Actions.RESOLVED_LOAD_CONTACT: {
            return {
                ...state,
                selectedContact: action.payload
            }
        }
        case Actions.RESOLVED_CREATE_CONTACT: {
            return {
                ...state,
                contacts: state.contacts.concat(action.payload)
            }
        }
        case Actions.RESOLVED_UPDATE_CONTACT: {
            return {
                ...state,
                selectedContact: action.payload
            }
        }
        case Actions.RESOLVED_DELETE_CONTACT: {
            return {
                ...state,
                selectedContact: null
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
}

export default rootReducer;