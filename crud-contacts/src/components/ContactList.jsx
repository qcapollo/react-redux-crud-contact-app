import React from 'react';
import { connect } from 'react-redux';
import shortid from 'shortid';
import { Collection } from 'react-materialize';

import ContactListItem from './ContactListItem';

import { resolvedLoadAllContacts } from '../actions/call_api_actions';

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts,
        searchText: state.searchText
    }
}

class ConnectedContactList extends React.Component {


    componentDidMount() {
        this.props.resolvedLoadAllContacts();
    }

    render() {
        let filtedContact;
        let text = this.props.searchText;
        if(text != null && text.length > 0){
            text = text.toLowerCase();
            filtedContact = this.props.contacts.filter((contact, i) => {
                return contact.name.toLowerCase().includes(text) || contact.phone.toLowerCase().includes(text)
            });
        } 
        else {
            filtedContact = this.props.contacts;
        }
        let list = filtedContact.map(contact => (
            <ContactListItem 
                key={shortid.generate()} 
                id={contact.id} 
                name={contact.name}
                phone={contact.phone} />
        ));
        return (
            <Collection>
                {list}
            </Collection>
        );
    }
}

const ContactList = connect(mapStateToProps, { resolvedLoadAllContacts }) (ConnectedContactList);
export default ContactList;