import React from 'react';
import { connect } from  'react-redux';
import { Input, Icon, Button } from 'react-materialize';

import { searchContact, clearSearchText } from '../actions/index';

import '../styles/css/searchbar.css'

const mapDispatchToProps = (dispatch) => {
    return {
        onChange: value => dispatch(searchContact(value)),
        clearSearchText: () => dispatch(clearSearchText())
    }
}

class ConnectedSearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearText = this.handleClearText.bind(this);
    }

    handleChange(event) {
        let value = event.target.value;
        this.props.onChange(value); //call dispatch
        this.setState({
            text: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        alert('sss');
    }

    handleClearText() {
        this.setState({
            text: ''
        });
        this.props.clearSearchText();
    }

    render() {
        let clearEnabled = this.state.text.length > 0;
        return (
            <div className="search-bar">
                <Input 
                    s={12} l={12} m={12}
                    id="searchbox"
                    type="text" 
                    label='Find people' 
                    onChange={this.handleChange}
                    value={this.state.text}/>

                {clearEnabled ? 
                    <Button waves='light' 
                        className="grey" 
                        onClick={this.handleClearText}>
                            <Icon center>clear</Icon>
                    </Button>
                    : null
                }
                
            </div>
        );
    }
}

const SearchBar = connect(null, mapDispatchToProps) (ConnectedSearchBar);
export default SearchBar;